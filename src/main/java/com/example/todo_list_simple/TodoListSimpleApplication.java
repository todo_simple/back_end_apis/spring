package com.example.todo_list_simple;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoListSimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoListSimpleApplication.class, args);
	}

}
