/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.controller;

import com.example.todo_list_simple.models.TodoListModel;
import com.example.todo_list_simple.controller.TodoListModelController;
import com.example.todo_list_simple.utils.ResponseApi;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.todo_list_simple.service.todo_list.TodoListInterface;
import com.example.todo_list_simple.utils.ResponseApi;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.stereotype.Controller;

/**
 *
 * @author BRANDO
 */
@RestController
@RequestMapping("/getTodoList")
//@CrossOrigin(origins = "*" , methods = {RequestMethod.GET,RequestMethod.POST,})
public class TodoListController {

    @Autowired
    private TodoListInterface todoListInterface;
    public ResponseApi responseApi = new ResponseApi();

    @ApiOperation(value = "Mensaje de bienvenida a la aplicacion",
            responseContainer = "Nothing")
    @GetMapping("/welcome")
    public String welcome() {
        return "Hello World";
    }

    @ApiOperation(value = "Se usa para añadir un nuevo item al TodoList",
            responseContainer = "Nothing")
    @PostMapping("/addNewItem")
    @CrossOrigin(origins = "*", methods = {RequestMethod.POST})
    public ResponseEntity<ResponseApi> addNewItem(@Valid @RequestBody TodoListModelController body) {

        //System.out.println("assa  " + body);

        TodoListModel obj = new TodoListModel();
        obj.setBody(body.getBody());
        
        //System.out.println("assa  " + obj.getBody());
        //System.out.println("asdasdasdas  " + obj.toString());

        boolean flag = todoListInterface.addNewItemList(obj);
        responseApi.setData(obj);
        responseApi.setCurrDate(new Date());

        if (flag == false) {
            responseApi.setMessage("Ya existe un item con el mismo nombre ");
            responseApi.setError(true);
            responseApi.setData(null);
            return new ResponseEntity<ResponseApi>(responseApi, HttpStatus.CONFLICT);
        }

        responseApi.setMessage("Se realizo con exito la operacion");
        responseApi.setError(false);
        return new ResponseEntity<ResponseApi>(responseApi, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Se usa obtener todos los items del TodoList",
            responseContainer = "Nothing")
    @GetMapping("/getAll")
    @CrossOrigin(origins = "*", methods = {RequestMethod.GET})
    public ResponseEntity<List<TodoListModel>> getAllItems() {
        // This returns a JSON or XML with the items

        List<TodoListModel> list = todoListInterface.getAllItemsList();
        return new ResponseEntity<List<TodoListModel>>(list, HttpStatus.OK);
    }

}
