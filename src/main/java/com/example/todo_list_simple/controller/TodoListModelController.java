/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.springframework.lang.Nullable;


/**
 * Entidad TodoListModelController.
 * @author BRANDO.
 * @version 1.0.0
 * @since 04-04-2019
 * @desc This is the entity class which Hibernate will automatically translate into a table.
 */


public class TodoListModelController {
    
    @JsonIgnore
    private long id;
    
    @JsonIgnore
    private long id_user;
    
    @Nullable
    private String header;
    
    // private final String body; // --- final para que sea obligatorio inicializarla
    private String body;
    
    
    
    // --------------------------------------- Constructors
    

    public TodoListModelController() {
    }
    
    public TodoListModelController(String body) {
        this.body = body;
    }

    public TodoListModelController(long id_user, String header, String body) {
        this.id_user = id_user;
        this.header = header;
        this.body = body;
    }

    
    
    
    // --------------------------------------- Get and Set
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id_user) {
        this.id_user = id_user;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", id_user=" + id_user + ", header=" + header + ", body=" + body + '}';
    }
    
    
    
    
   
    

}
