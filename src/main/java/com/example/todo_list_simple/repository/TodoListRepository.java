/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.repository;

import com.example.todo_list_simple.controller.TodoListController;
import org.springframework.stereotype.Repository;
import com.example.todo_list_simple.models.TodoListModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author BRANDO
 */
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
@Repository
public interface TodoListRepository extends JpaRepository<TodoListModel, Long> {

    List<TodoListModel> findByBody(String itemBody); // Por defecto spring reconoce la columna body a si que no es necesario crear consultas 

    List<TodoListModel> findByHeader(String itemHeader);


    /**
     * Metodo para obtener el item especifico de la lista por el body
     *
     * @param itemBody Parametro de tipo String.
     * @return Un objeto tipo TodoListModel.
     */
//    @Query("SELECT ts FROM TodoListModel ts WHERE  ts.body = ?1") // se puede utilizar el mismo modelo creado 
    @Query(value = "SELECT ts.* FROM todo_simple ts WHERE  ts.body = ?1", nativeQuery = true) // para cuando quieres hacer consultas sin el modelo creado
    List<TodoListModel> findBody(String itemBody);

    
}
