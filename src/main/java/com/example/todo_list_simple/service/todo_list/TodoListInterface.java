/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.service.todo_list;

import com.example.todo_list_simple.models.TodoListModel;
import java.util.List;

/**
 *
 * @author BRANDO
 */
public interface TodoListInterface {
    
     List<TodoListModel> getAllItemsList();
     TodoListModel getItemById(long itemlistId);
     boolean addNewItemList(TodoListModel itemlist);
     void updateItemList(TodoListModel itemlist);
     void deleteItemList(long itemlistId);
}
