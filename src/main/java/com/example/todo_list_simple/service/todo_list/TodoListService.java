/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.service.todo_list;

import com.example.todo_list_simple.repository.TodoListRepository;
import com.example.todo_list_simple.models.TodoListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author BRANDO
 */
@Service
public class TodoListService implements TodoListInterface {

    @Autowired // This means to get the bean called TodoListRepository
    private TodoListRepository todoSimpleRepository;
    
    
    

    @Override
    public List<TodoListModel> getAllItemsList() {
        List<TodoListModel> list = new ArrayList<>();
        todoSimpleRepository.findAll().forEach(e -> list.add(e));
        return list;
    }

    @Override
    public TodoListModel getItemById(long itemId) {
        TodoListModel obj = todoSimpleRepository.findById(itemId).get();
        return obj;
    }

    @Override
    public boolean addNewItemList(TodoListModel itemlist) {

        List<TodoListModel> list = todoSimpleRepository.findByBody(itemlist.getBody());
        //System.out.println("Tamaño de la lista : "+ list.size());
        if (list.size() > 0) {
            return false;
        }

        Object s = todoSimpleRepository.save(itemlist);
//        System.out.println("Hola mundo : "+s.toString());
        
        return true;
    }

    @Override
    public void updateItemList(TodoListModel itemlist) {
        todoSimpleRepository.save(itemlist);
    }

    @Override
    public void deleteItemList(long itemlistId) {
        todoSimpleRepository.delete(getItemById(itemlistId));
    }

    

}
