/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.swagger;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author BRANDO
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                ;
    }

    /**
     * Este metodo permite entregar informacion sobre el API.
     *
     * @return ApiInfo para indicarle al SWAGGER para que sirve el API.
     */
    private ApiInfo apiInfo() {
        ApiInfo info = new ApiInfo(
                "API para realizar un todo list.",
                "A traves de esta podran realizar operaciones respecto a la aplicacion",
                "API 1.0",
                "Terms of service",
                new Contact("Jose Brandon Henao", "Ingeniero de sistemas",
                        "jbhenao17@misena.edu.co"),
                "License of API", "API license URL"
        );

        return info;
    }
}
