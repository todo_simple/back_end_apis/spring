/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.todo_list_simple.utils;

import java.util.Date;

/**
 *
 * @author BRANDO
 */
public class ResponseApi {

    /**
     * Mensaje de respuesta.
     */
    private String message;

    /**
     * Fecha de la respuesta.
     */
    private Date currDate = new Date(); // --- por defecto tiene una fecha la que se crea la clase

    /**
     * Si la respuesta tiene error
     */
    private boolean error = false; // --- por defecto es falso es decir que no tiene error

    /**
     * La informacion solicitados
     */
    private Object data;

    // ----------------------------------------------------------------------------------- Contructor
    public ResponseApi(String message, Date currDate, boolean error, Object data) {
        this.message = message;
        this.currDate = currDate;
        this.error = error;
        this.data = data;
    }

    public ResponseApi(String message, Date currDate, Object data) {
        this.message = message;
        this.currDate = currDate;
        this.data = data;
    }

    public ResponseApi() {
    }
    
    

    // ----------------------------------------------------------------------------------- Get and Set
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCurrDate() {
        return currDate;
    }

    public void setCurrDate(Date currDate) {
        this.currDate = currDate;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
